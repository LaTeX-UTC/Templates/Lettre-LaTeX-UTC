\ProvidesClass{lettre-UTC}
\LoadClass{article}

\usepackage{Graphismes-UTC/couleurs/couleurs_UTC}


\usepackage[a4paper, left=55mm, right=10mm, top=47mm, bottom=45mm]{geometry}
    
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\renewcommand{\familydefault}{\sfdefault}
\usepackage{atbegshi}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{lastpage}
\usepackage{hyperref}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% éléments par défauts
\def\adresse{%
Pas d'adresse inscite.%
Merci d'utiliser la comande \texttt{\@backslashchar adresse} dans le préambule.%
}

\def\letterDate{Pas de date ?}

\def\letterInfo{Pas d'information.}

\def\heightBlockInfo{25mm}

\def\infosPerso{Où sont les infos ??}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Graphismes

\newcommand\print@Fleche{%
\ifnum\value{page}>1\relax
    \begin{tikzpicture}[overlay, remember picture]
        \node[anchor=north east, 
                xshift=200mm, %shifting around
                yshift=-10mm, inner sep = 0pt] 
            at (current page.north west) %left upper corner of the page
            {\includegraphics[height=10mm]{./Graphismes-UTC/elements/fleche/fleche.pdf}}; 
    \end{tikzpicture}  
\fi
}


\newcommand\print@LogoUTC{
\ifnum\value{page}=1\relax
    \begin{tikzpicture}[overlay, remember picture]
        \node[anchor=north east, %anchor is upper left corner of the graphic
              xshift=52.2mm, %shifting around
              yshift=-10mm, inner sep = 0pt] 
             at (current page.north west) %left upper corner of the page
             {\includegraphics[width=42.2mm]{./Graphismes-UTC/logos/UTC/logo_UTC.pdf}}; 
        \end{tikzpicture}
\fi          
}


\newcommand\printLigne{%
    \begin{tikzpicture}[overlay, remember picture]
        \node[anchor=north west, 
            xshift=0mm, %shifting around
            yshift=-99mm, inner sep = 0mm] 
        at (current page.north west) %left upper corner of the page
        {\tikz \draw [line width = 0.2mm] (0,0) -- (13mm,0mm);};
    \end{tikzpicture}    
}


\newcommand\print@Footer{%
\begin{tikzpicture}[overlay, remember picture]
    % rectangle jaune
    \node[anchor=north west, %anchor is upper left corner of the graphic
            xshift=1cm, %shifting around
            yshift=-277mm, inner sep = 0pt] 
        at (current page.north west) %left upper corner of the page
        {\tikz \fill [jauneUTC] (0,0) rectangle (19cm,1cm); }; 
    
    % page number
    \node[anchor=north east, %anchor is upper left corner of the graphic
            xshift=200mm, %shifting around
            yshift=-272mm, inner sep = 0pt] 
        at (current page.north west) %left upper corner of the page
        {\textbf{\thepage/\pageref{LastPage}} }; 
\end{tikzpicture}%
}



\newcommand\printAdress{
    \ifnum\value{page}=1\relax
        \begin{tikzpicture}[overlay, remember picture]
            \node[anchor=north west, 
                xshift=110mm, %shifting around
                yshift=-47mm,text width=80mm, inner sep = 0pt]
            at (current page.north west) %left upper corner of the page
            {\adresse}; 
        \end{tikzpicture}
    \fi
}

\newcommand\blockInfosUTC{%
\ifnum\value{page}=1\relax
    \begin{tikzpicture}[overlay, remember picture]

        % rectangle plein
        \node[anchor=north west, 
                xshift=10mm, %shifting around
                yshift=-227mm, inner sep = 0pt] 
            at (current page.north west) %left upper corner of the page
            {\tikz \fill [jauneClairUTC] (0,0) rectangle (42mm,50mm);};
        
        % contenu
        \node[anchor=north west, 
                xshift=12mm, %shifting around
                yshift=-227mm,text width=42mm, inner sep = 0pt]
            at (current page.north west) %left upper corner of the page
            {%
                \vspace*{3.5mm}\\%
                \textcolor{grisUTC}{%
                    \textbf{Université de\\%
                    Techologie \\%
                    de Compiègne}%
                    \bigskip \\%
                    CS 60 319\\%
                    \small{Rue du Docteur Schweitzer}\\%
                    60 203 Compiègne cedex
                    \bigskip \\%
                    tél: 03 44 23 44 23\\%
                    \textbf{\href{www.utc.fr}{www.utc.fr}}%
                }%
            }; 
    \end{tikzpicture}      
\fi  
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Blocks de texte

\newcommand\blockInfosPerso{%
\ifnum\value{page}=1\relax
    \begin{tikzpicture}[overlay, remember picture]
        
        % rectangle
        \node[anchor=south west, 
            xshift=10mm, %shifting around
            yshift=-228mm, inner sep = 0mm] 
        at (current page.north west) %left upper corner of the page
        {\tikz \draw [jauneClairUTC, line width = 1mm] (0,0) rectangle (41mm,\heightBlockInfo);};
        
        % contenu
        \node[anchor=south west, 
            xshift=12mm, %shifting around
            yshift=-228mm,text width=42mm, inner sep = 0pt]
            at (current page.north west) %left upper corner of the page
            {\infosPerso}; 
    \end{tikzpicture}  
\fi  
}



\newcommand\printLetterInfo{%
\ifnum\value{page}=1\relax
\begin{tikzpicture}[overlay, remember picture]
    \node[anchor=north west, 
         xshift=10mm, %shifting around
         yshift=-47mm,text width=42mm, inner sep = 0pt]
        at (current page.north west) %left upper corner of the page
        {\letterDate\\ \bigskip %
        \letterInfo}; 
\end{tikzpicture}
\fi  
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Génération


\pagestyle{empty}
\AtBeginShipout{%
\AtBeginShipoutAddToBox{
    \printAdress%
    \printLetterInfo%
    \print@LogoUTC%
    \print@Fleche%
    \printLigne%
    \blockInfosUTC%
    \blockInfosPerso%
    \print@Footer}%
}


\AtBeginDocument{
    \vspace*{5cm} % Hack pour la première page
}